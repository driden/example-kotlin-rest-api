import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
	mavenCentral()
	jcenter()
}

plugins {
	val kotlinVersion = "1.4.10"
	kotlin("jvm") version kotlinVersion
	kotlin("plugin.allopen") version kotlinVersion
	kotlin("plugin.spring") version kotlinVersion
	kotlin("plugin.jpa") version kotlinVersion
	kotlin("kapt") version kotlinVersion
	id("nu.studer.jooq") version "5.2"
	id("org.springframework.boot") version "2.2.4.RELEASE"
	id("io.spring.dependency-management") version "1.0.10.RELEASE"
	id("idea")
	id("java")
	id("io.ebean") version "12.5.1"
}

ebean {
	debugLevel = 1 //0 - 9
}

jooq {
	version.set("3.14.3")
	edition.set(nu.studer.gradle.jooq.JooqEdition.OSS)
	configurations {
		create("main") {
			jooqConfiguration.apply {
				logging = org.jooq.meta.jaxb.Logging.WARN
				jdbc.apply {
					driver = "org.postgresql.Driver"
					url = "jdbc:postgresql://localhost:5444/testscmdb"
					user = "testdbadmin"
					password = "Vu20190418"
//					properties.add(Property().withKey("PAGE_SIZE").withValue("2048"))
				}
				generator.apply {
					name = "org.jooq.codegen.JavaGenerator"
					database.apply {
						name = "org.jooq.meta.postgres.PostgresDatabase"
//						forcedTypes.addAll(arrayOf(
//								ForcedType()
//										.withName("varchar")
//										.withIncludeExpression(".*")
//										.withIncludeTypetargets("JSONB?"),
//								ForcedType()
//										.withName("varchar")
//										.withIncludeExpression(".*")
//										.withIncludeTypes("INET")
//						).toList())
						inputSchema = "public"
					}
					generate.apply {
						isDeprecated = false
						isRecords = false
						isImmutablePojos = false
						isFluentSetters = false
					}
					target.apply {
						packageName = "cube.tables"
						directory = "src/generated/jooq"
					}
					strategy.name = "org.jooq.codegen.DefaultGeneratorStrategy"
				}
			}
		}
	}
}

allOpen {
	annotation("javax.persistence.Entity")
	annotation("javax.persistence.Embeddable")
	annotation("javax.persistence.MappedSuperclass")
}

group = "io.tradewindow"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_1_8

configurations {
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}


dependencies {
	implementation("com.fasterxml.jackson.core:jackson-databind:2.11.3")

	implementation("org.postgresql:postgresql:42.2.18")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springframework.boot:spring-boot-devtools")
	implementation("org.springframework.boot:spring-boot-starter-data-rest") {
		exclude(group = "org.springframework.boot", module = "spring-boot-starter-tomcat")
	}
	implementation("org.springframework.boot:spring-boot-starter-webflux:2.4.0")
	implementation("org.springframework.boot:spring-boot-starter-jdbc")
	implementation("org.jooq:jooq:3.14.3")
	implementation("org.jooq:jooq-codegen:3.14.3")
	implementation("io.ebean:ebean:12.5.1")
	implementation("io.ebean:ebean-querybean:12.5.1")
	annotationProcessor("io.ebean:querybean-generator:12.5.1")
	testImplementation("io.ebean:ebean-test:12.5.1")
	implementation("org.hibernate:hibernate-entitymanager:5.4.23.Final")
	jooqGenerator("org.postgresql:postgresql:42.2.18")
//	testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}
	//	implementation("org.springframework.boot:spring-boot-starter-data-jpa")
	//	implementation("org.hibernate:hibernate-core:5.4.23.Final")
	//	implementation("org.datanucleus:datanucleus-core:5.2.4")
	//	implementation("org.datanucleus:datanucleus-api-jpa:5.2.4")
	//	implementation("org.datanucleus:javax.persistence:2.2.2")
	//	implementation("org.datanucleus:datanucleus-accessplatform-jdo-rdbms:5.2.4")
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.named<nu.studer.gradle.jooq.JooqGenerate>("generateJooq") { allInputsDeclared.set(true) }