package io.tradewindow.system.cube.repository

import cube.tables.Tables.*
import io.ebean.BeanRepository
import io.ebean.Database
import io.tradewindow.system.cube.dto.DBusinessCategory
import io.tradewindow.system.cube.dto.DIndustryType
import io.tradewindow.system.cube.dto.DOrganisation
import io.tradewindow.system.cube.entity.Organisation
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.Query
import org.jooq.impl.DSL.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Repository
import java.util.*


@Repository
class OrganisationRepository @Autowired constructor(db: Database?) : BeanRepository<UUID, Organisation>(Organisation::class.java, db) {
    @Autowired
    @Qualifier("dsl")
    lateinit var ctx: DSLContext

    fun queryOrganisations(): List<Any> {
        val countAccount: Field<DOrganisation?> = ctx.selectCount()
                .from(V2_ACCOUNT)
                .where(V2_ACCOUNT.ORGANISATION_ID.eq(V2_ORGANISATION.ID))
                .asField("accounts")
        val dBusinessCategorySub: Field<DBusinessCategory?> = ctx.select(
                jsonObject(
                        key(field("'id'", String::class.java)).value(V2_BUSINESS_CATEGORY.ID),
                        key(field("'name'", String::class.java)).value(V2_BUSINESS_CATEGORY.NAME),
                        key(field("'code'", String::class.java)).value(V2_BUSINESS_CATEGORY.CODE)
                ))
                .from(V2_BUSINESS_CATEGORY).leftJoin(V2_ORGANISATION_BUSINESS_CATEGORY)
                .on(V2_BUSINESS_CATEGORY.ID.eq(V2_ORGANISATION_BUSINESS_CATEGORY.BUSINESS_CATEGORY_ID))
                .where(V2_ORGANISATION.ID.eq(V2_ORGANISATION_BUSINESS_CATEGORY.ORGANISATION_ID)).asField("business_category")
        val dIndustryTypeSub: Field<DIndustryType?> = ctx.select(
                jsonObject(
                        key(field("'id'", String::class.java)).value(V2_INDUSTRY_TYPE.ID),
                        key(field("'name'", String::class.java)).value(V2_INDUSTRY_TYPE.NAME)
                ))
                .from(V2_INDUSTRY_TYPE)
                .where(V2_ORGANISATION.INDUSTRY_TYPE_ID.eq(V2_INDUSTRY_TYPE.ID)).asField("industry_type")
        val issuerSub: Field<DOrganisation?> = ctx.select(
                jsonObject(
                        key(field("'id'", String::class.java)).value(V2_ORGANISATION.ID),
                        key(field("'name'", String::class.java)).value(V2_ORGANISATION.NAME)
                ))
                .from(V2_ORGANISATION)
                .where(V2_ORGANISATION.ISSUER.eq(V2_ORGANISATION.ID)).asField("issuer")
        val query: Query = ctx.select(
            V2_ORGANISATION.ID,
            V2_ORGANISATION.NAME,
            V2_ORGANISATION.DISABLED,
            V2_ORGANISATION.VERIFIED,
            V2_ORGANISATION.CREATED_AT,
            V2_ORGANISATION.UPDATED_AT,
            dBusinessCategorySub,
            dIndustryTypeSub,
            countAccount,
            issuerSub
        ).from(V2_ORGANISATION)

        val queryBase = query.sql
        return server.findDto(DOrganisation::class.java, queryBase).setRelaxedMode().findList()
    }
    private fun whereGenerator() {
        val condition = noCondition()
        
    }
}