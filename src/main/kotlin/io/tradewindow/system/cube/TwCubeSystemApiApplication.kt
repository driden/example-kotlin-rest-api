package io.tradewindow.system.cube

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class TwCubeSystemApiApplication

fun main(args: Array<String>) {
	runApplication<TwCubeSystemApiApplication>(*args)
}
