package io.tradewindow.system.cube.controller.impl

import io.tradewindow.system.cube.controller.OrganisationController
import io.tradewindow.system.cube.request.QOrganisation
import io.tradewindow.system.cube.entity.Organisation
import io.tradewindow.system.cube.service.OrganisationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.RestController
import java.util.*

@RestController
class OrganisationControllerImpl: OrganisationController {
    @Autowired
    private lateinit var organisationService: OrganisationService

    override fun getOrganisationById(id: UUID): Organisation? {
        return organisationService.getOrganisationById(id)
    }

    override fun getOrganisations(queries: QOrganisation): List<Any?> {
        println("===>>>>>>>0"+ (queries.name))
        println("===>>>>>>>1"+ (queries.issuing_body?.name))
        var lists  = organisationService.listOrganisations() as List<Organisation>;
        for (org in lists) {
            if (org.industryType?.id!=null) {
                println("===>>>>>>>>>>${org.industryType?.id}")
            }
        }
        return lists;
//        return organisationService.listOrganisations()
    }

    override fun createOrganisation() {
        TODO("Not yet implemented")
    }

    override fun deleteOrganisationById(id: String) {
        TODO("Not yet implemented")
    }

    override fun enableOrganisationAvailability(id: String) {
        TODO("Not yet implemented")
    }

    override fun disableOrganisationAvailabilityById(id: String) {
        TODO("Not yet implemented")
    }

    override fun patchOrganisationDetailById(id: String) {
        TODO("Not yet implemented")
    }

    override fun updateOrganisationDetailById(id: String) {
        TODO("Not yet implemented")
    }


}