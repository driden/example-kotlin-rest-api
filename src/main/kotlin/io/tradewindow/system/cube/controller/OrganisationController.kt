package io.tradewindow.system.cube.controller

import io.tradewindow.system.cube.request.QOrganisation
import io.tradewindow.system.cube.entity.Organisation
import org.springframework.web.bind.annotation.*
import java.util.*

@RequestMapping("/system/v2/organisation_management/organisations")
interface OrganisationController {
    @GetMapping("/{id}")
    fun getOrganisationById(@PathVariable id: UUID): Organisation?
    @GetMapping("")
    fun getOrganisations(queries: QOrganisation): List<Any?>
    @PostMapping("/")
    fun createOrganisation()
    @DeleteMapping("/{id}")
    fun deleteOrganisationById(@PathVariable id:String)
    @PatchMapping("/{id}/enable")
    fun enableOrganisationAvailability(@PathVariable id:String)
    @PatchMapping("/{id}/disable")
    fun disableOrganisationAvailabilityById(@PathVariable id:String)
    @PatchMapping("/{id}")
    fun patchOrganisationDetailById(@PathVariable id:String)
    @PutMapping("/{id}")
    fun updateOrganisationDetailById(@PathVariable id:String)
}