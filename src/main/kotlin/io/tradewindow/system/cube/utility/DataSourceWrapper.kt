package io.tradewindow.system.cube.utility

import org.springframework.stereotype.Component
import java.util.*

class DataSourceWrapper(properties: Properties, prefix: String) {
    private val prefixK: String = if(prefix==null) "" else "$prefix."
    private val properties = properties
    fun getDriver(): String {
        return this.properties["${prefixK}driver"] as String
    }
    fun getUrl(): String {
        return this.properties["${prefixK}url"] as String
    }
    fun getUserName(): String {
        return this.properties["${prefixK}username"] as String
    }
    fun getPassword(): String {
        return this.properties["${prefixK}password"] as String
    }
    fun getDialect(): String {
        return this.properties["${prefixK}dialect"] as String
    }
}