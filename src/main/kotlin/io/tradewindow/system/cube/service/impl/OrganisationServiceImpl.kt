package io.tradewindow.system.cube.service.impl

import io.ebean.DB
import io.ebean.Database
import io.tradewindow.system.cube.entity.Organisation
import io.tradewindow.system.cube.repository.OrganisationRepository
import io.tradewindow.system.cube.service.OrganisationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.*


@Service
class OrganisationServiceImpl: OrganisationService {

    @Autowired
    private lateinit var organisationRepository: OrganisationRepository

    override fun getOrganisationById(id: UUID): Organisation? {

        return organisationRepository.findById(id)
    }

    override fun listOrganisations(): List<Any> {
//        return organisationRepository.queryOrganisations()
        return organisationRepository.findAll()
//        val db = DB.byName("primary")
//        return db.find(Organisation::class.java).findList()
//        return result
    }

    override fun createOrganisation(organisation: Organisation): Organisation {
        TODO()
    }

    override fun deleteById(id: String) {
        TODO("Not yet implemented")
    }

    override fun updateOrganisation(id: String) {
        TODO("Not yet implemented")
    }

    override fun updateManyOrganisations() {
        TODO("Not yet implemented")
    }

}