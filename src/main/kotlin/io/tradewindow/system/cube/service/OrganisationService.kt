package io.tradewindow.system.cube.service

import io.tradewindow.system.cube.entity.Organisation
import java.util.*

interface OrganisationService {
    fun getOrganisationById(id: UUID): Organisation?
    fun listOrganisations(): List<Any>
    fun createOrganisation(organisation: Organisation): Organisation
    fun deleteById(id:String)
    fun updateOrganisation(id:String)
    fun updateManyOrganisations()
}