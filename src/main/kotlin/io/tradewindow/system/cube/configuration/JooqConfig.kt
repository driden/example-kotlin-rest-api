package io.tradewindow.system.cube.configuration

import io.tradewindow.system.cube.utility.DataSourceWrapper
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.conf.Settings
import org.jooq.impl.DSL
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.sql.Connection
import javax.sql.DataSource

@Configuration
class JooqConfig {
    @Bean("dsl")
    @Autowired
    fun getDSLContext(@Qualifier("datasource") dataSource: DataSource, dsw: DataSourceWrapper):DSLContext {
//        val connection: Connection = DriverManager.getConnection(datasourceProperties.url, datasourceProperties.userName, datasourceProperties.password)
        val connection: Connection = dataSource.connection
        return context(connection, dsw.getDialect())
    }

    @Bean("dsl-pretty-print")
    @Autowired
    fun getDSLContextPrettyPrint(@Qualifier("datasource") dataSource: DataSource, dsw: DataSourceWrapper):DSLContext {
//        val connection: Connection = DriverManager.getConnection(datasourceProperties.url, datasourceProperties.userName, datasourceProperties.password)
        val connection: Connection = dataSource.connection
        return context(connection, dsw.getDialect(), Settings().withRenderFormatted(true))
    }

    private fun dialectConverter(keyword: String): SQLDialect {
        return when(keyword) {
            "org.hibernate.dialect.PostgreSQL95Dialect" -> SQLDialect.POSTGRES
            else -> throw Exception("Not supported dialect $keyword")
        }
    }
    fun context(conn: Connection, dialectStr: String): DSLContext {
        return this.context(conn, this.dialectConverter(dialectStr))
    }
    fun context(conn: Connection, dialect: SQLDialect): DSLContext {
        return DSL.using(conn, dialect)
    }
    fun context(conn: Connection, dialectStr: String, settings: Settings): DSLContext {
        return DSL.using(conn, this.dialectConverter(dialectStr), settings)
    }
}