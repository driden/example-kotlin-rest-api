package io.tradewindow.system.cube.configuration

import org.springframework.stereotype.Component

@Component("datasource-meta")
object DataSourceMeta {
    const val fileName = "datasource.properties"
    const val prefix = "datasource.db"
    const val configName = "default"
}
