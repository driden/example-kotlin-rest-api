package io.tradewindow.system.cube.configuration

import io.tradewindow.system.cube.utility.DataSourceWrapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import java.util.*
import javax.sql.DataSource


@Configuration
class DataSourceConfig {
    @Bean("datasource")
    @Autowired
    fun getDataSource(dsw: DataSourceWrapper): DataSource {
        val dataSourceBuilder = DataSourceBuilder.create()
        dataSourceBuilder.driverClassName(dsw.getDriver())
        dataSourceBuilder.url(dsw.getUrl())
        dataSourceBuilder.username(dsw.getUserName())
        dataSourceBuilder.password(dsw.getPassword())
        return dataSourceBuilder.build()
    }
}