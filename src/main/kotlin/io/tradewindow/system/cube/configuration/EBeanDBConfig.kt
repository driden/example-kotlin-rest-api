package io.tradewindow.system.cube.configuration

import io.ebean.Database
import io.ebean.DatabaseFactory
import io.ebean.config.DatabaseConfig
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource


@Configuration
class EBeanDBConfig {
    @Autowired
    @Qualifier("datasource-meta")
    lateinit var meta: DataSourceMeta
    @Bean
    @Autowired
    fun getDatabase(@Qualifier("datasource") dataSource: DataSource): Database {
        val dbConfig = DatabaseConfig()
        dbConfig.name = meta.configName
        dbConfig.dataSource = dataSource
        return DatabaseFactory.create(dbConfig)
    }
}