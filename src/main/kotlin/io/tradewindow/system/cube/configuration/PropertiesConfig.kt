package io.tradewindow.system.cube.configuration

import io.ebean.Database
import io.ebean.DatabaseFactory
import io.ebean.config.DatabaseConfig
import io.tradewindow.system.cube.utility.DataSourceWrapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.io.ClassPathResource
import java.util.*
import javax.sql.DataSource


@Configuration
class PropertiesConfig {
    @Bean("datasource-properties")
    @Autowired
    fun getProperties(meta: DataSourceMeta): Properties {
        val properties = Properties()
        val inputStream = ClassPathResource(meta.fileName).inputStream
        properties.load(inputStream)
        properties.forEach {(k, v)-> println("key=$k, value=$v")}
        return properties
    }
    @Bean
    @Autowired
    fun getDataSourceWrapper(@Qualifier("datasource-properties") properties: Properties, meta: DataSourceMeta): DataSourceWrapper {
        return DataSourceWrapper(properties, meta.prefix)
    }
}