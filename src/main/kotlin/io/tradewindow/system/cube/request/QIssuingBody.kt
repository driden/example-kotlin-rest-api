package io.tradewindow.system.cube.request

class QIssuingBody {
    var name: Map<String, String>? = null
    var code: Map<String, String>? = null
}