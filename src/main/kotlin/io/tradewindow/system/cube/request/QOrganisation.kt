package io.tradewindow.system.cube.request

import javax.validation.constraints.NotBlank

class QOrganisation {
    @NotBlank
    var name: Map<String, String>? = null
    @NotBlank
    var issuing_body: QIssuingBody? = null
}