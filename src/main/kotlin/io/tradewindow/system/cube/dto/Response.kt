package io.tradewindow.system.cube.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.JsonRootName
import java.util.*

@JsonRootName("response")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {
    @JsonProperty("status_code")
    var statusCode: Int = 0
    lateinit var message: String
    lateinit var timestamp: Date
    lateinit var result: Any
}
