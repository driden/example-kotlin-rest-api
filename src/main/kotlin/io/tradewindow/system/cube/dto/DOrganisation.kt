package io.tradewindow.system.cube.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.databind.ObjectMapper
import java.util.*
import javax.persistence.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class DOrganisation {
    var id: UUID? = null
    var name: String? = null
    var type: String? = null


    @JsonProperty("issuer")
    private var issuer: String? = null
    fun setIssuer(issuerStr : String?) {
        this.issuer = issuerStr
    }
    fun getIssuer(): DOrganisation? {
        val mapper = ObjectMapper()
        if (this.issuer==null) return null
        return mapper.readValue(this.issuer, DOrganisation::class.java)
    }

    private var businessCategory: String? = null
    fun setBusinessCategory(businessTypeStr : String?) {
        this.businessCategory = businessTypeStr
    }
    @JsonProperty("business_category")
    fun getBusinessCategory(): DBusinessCategory? {
        val mapper = ObjectMapper()
        if (this.businessCategory==null) return null
        return mapper.readValue(this.businessCategory, DBusinessCategory::class.java)
    }

    @JsonProperty("industry_type_id")
    private var industryTypeId: UUID? = null

    private var industryType: String? = null
    fun setIndustryType(industryTypeStr : String?) {
        this.industryType = industryTypeStr
    }
    @JsonProperty("industry_type")
    fun getIndustryType(): DIndustryType? {
        val mapper = ObjectMapper()
        if (this.industryType==null) return null
        return mapper.readValue(this.industryType, DIndustryType::class.java)
    }

    var accounts: Int? = null

    @JsonProperty("disabled")
    var disabled: Boolean? = null
    @JsonProperty("verified")
    var verified: Boolean? = null

    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null

    //    @Column(name = "last_commit")
    //    var lastCommit: String?=""

    //    @OneToOne(mappedBy = "contact_id")
    //    lateinit var contact:Contact

    //    @OneToOne(mappedBy = "notification_id")
    //    lateinit var notification:NotificationOption
}