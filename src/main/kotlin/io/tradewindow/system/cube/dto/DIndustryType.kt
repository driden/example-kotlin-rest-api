package io.tradewindow.system.cube.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class DIndustryType() {
    var id: UUID? = null
    var name: String? = null
//    @Column(name = "is_disabled")
//    var isDisabled: Boolean = false
    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null
}