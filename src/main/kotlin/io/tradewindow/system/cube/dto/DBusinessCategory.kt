package io.tradewindow.system.cube.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

@JsonInclude(JsonInclude.Include.NON_NULL)
class DBusinessCategory() {
    var id: UUID? = null
    var name: String? = null
    var code: String? = null
}