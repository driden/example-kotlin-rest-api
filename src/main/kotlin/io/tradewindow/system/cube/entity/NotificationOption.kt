package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

//@Entity(name="notification_option")
//@Table(name="v2_notification_option")
@JsonInclude(JsonInclude.Include.NON_NULL)
class NotificationOption(referenceId: String): Model() {
    @Id
    @Column(name = "notification_id")
    var id: String? = null
    var email: String = ""
    var all: Boolean = false
    var contract: Boolean = false
    var document: Boolean = false
    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null
}