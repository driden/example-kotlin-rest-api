package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

@Entity(name="organisation_business_category")
@Table(name="v2_organisation_business_category")
@JsonInclude(JsonInclude.Include.NON_NULL)
class OrganisationBusinessCategory(name: String, code: String): Model() {
    @Id
    @Column(name = "organisation_id")
    var organisationId: UUID? = null
    @Column(name = "business_Category_id")
    var businessCategoryId: UUID? = null
}