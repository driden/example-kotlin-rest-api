package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

//@Entity(name="contact")
//@Table(name="v2_contact")
@JsonInclude(JsonInclude.Include.NON_NULL)
class Contact(referenceId: String): Model() {
    @Id
    @Column(name = "contract_id")
    var id: String? = null
    var email: String? = null
    var phone: String? = null
    var mobile: String? = null
    var fax: String? = null
    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null
}