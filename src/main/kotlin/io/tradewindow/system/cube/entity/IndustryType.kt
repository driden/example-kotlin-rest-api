package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

@Entity(name="industry_type")
@Table(name="v2_industry_type")
@JsonInclude(JsonInclude.Include.NON_NULL)
class IndustryType(name: String): Model() {
    @Id
    @Column(name = "id")
    var id: UUID? = null
    var name: String? = name
    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null
}