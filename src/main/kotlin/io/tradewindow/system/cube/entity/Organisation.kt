package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

@Entity(name="organisation")
@Table(name="v2_organisation")
@JsonInclude(JsonInclude.Include.NON_NULL)
class Organisation(name: String, type:String): Model() {
    @Id
    @Column(name = "id")
    var id: UUID? = null
    var name: String? = name
    var type: String? = type
    var issuer: UUID? = null
    @Column(name = "industry_type_id")
    @JsonProperty("industry_type_id")
    var industryTypeId: UUID? = null

    @ManyToOne(optional = false)
    @JoinColumn(name = "industry_type_id")
    @JsonProperty("industry_type")
    var industryType: IndustryType? = null

    @OneToOne(cascade = [CascadeType.ALL], fetch = FetchType.LAZY)
    @JoinTable(
        name = "v2_organisation_business_category",
        joinColumns = [JoinColumn(name = "organisation_id", referencedColumnName = "organisation_id", unique = true)],
        inverseJoinColumns = [JoinColumn(name = "business_category_id", referencedColumnName = "business_category_id", unique = true)]
    )
    @JsonProperty("business_category")
    @JsonIgnoreProperties("business_category")
    var businessCategory: BusinessCategory? = null

    @Column(name = "disabled")
    @JsonProperty("disabled")
    var disabled: Boolean? = null

    @Column(name = "verified")
    @JsonProperty("verified")
    var verified: Boolean? = null

//    @OneToOne(mappedBy = "contact_id")
//    lateinit var contact:Contact

    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null
}