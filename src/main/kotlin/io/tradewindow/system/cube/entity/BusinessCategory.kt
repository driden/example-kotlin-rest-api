package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

@Entity(name="business_category")
@Table(name="v2_business_category")
@JsonInclude(JsonInclude.Include.NON_NULL)
class BusinessCategory(name: String, code: String): Model() {
    @Id
    @Column(name = "id")
    var id: UUID? = null
    var name: String? = name
    var code: String? = code
    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null

    @OneToOne(mappedBy = "businessCategory")
    @JsonIgnoreProperties("businessCategory")
    val organisation: Organisation? = null
}