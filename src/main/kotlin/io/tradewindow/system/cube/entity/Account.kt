package io.tradewindow.system.cube.entity

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import io.ebean.Model
import java.util.*
import javax.persistence.*

//@Entity(name="account")
//@Table(name="v2_account")
@JsonInclude(JsonInclude.Include.NON_NULL)
class Account(email: String, firstName:String, lastName:String, organisationId: UUID, type: String = "ORG_USER"): Model() {
    @Id
    @Column(name = "id")
    var id: UUID? = null
    @Column(name = "email")
    @JsonProperty("account_name")
    var accountName: String? = email
    var password: String? = ""
    @Column(name = "first_name")
    var firstName: String? = firstName
    @Column(name = "last_name")
    var lastName: String? = lastName
    var type: String? = type
    @Column(name = "is_disabled")
    @JsonProperty("is_disabled")
    var isDisabled: Boolean? = null

//    @OneToOne(mappedBy = "contact_id")
//    lateinit var contact:Contact
    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id", nullable=false, insertable=false, updatable=false)
    var organisation: Organisation? = null
    @Column(name = "organisation_id")
    var organisationId: UUID? = organisationId

    @Column(name = "created_at")
    @JsonProperty("created_at")
    var createdAt: Date? = null
    @Column(name = "updated_at")
    @JsonProperty("updated_at")
    var updatedAt: Date? = null
}